﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowOxygenBottles : MonoBehaviour {

    private float timer;
    private float rate = 6;

    void Start () {
        timer = rate;
        gameObject.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
        if (timer >= rate)
        {
            gameObject.SetActive(true);
            timer = 0;
        }
    }

    public void ActiveBottle()
    {
        gameObject.SetActive(true);
    }
}
