﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RotateAnimation : MonoBehaviour
{
    public Slider mySlider;
    float rate = 0.06f;
    private SpriteRenderer mySpriteRenderer;
    float timer;
    public bool startBlinking = false;
    float stopTimer = 0;
    private AudioSource sound;

    // Use this for initialization
    void Start()
    {
        mySpriteRenderer = GetComponent<SpriteRenderer>();
        sound = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {

        if (startBlinking)
        {
            stopTimer += Time.deltaTime;
            BlinkingObject();
        }
        if (stopTimer >= 2f)
        {
            stopTimer = 0;
            mySpriteRenderer.enabled = true;
            startBlinking = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            sound.Play();
            mySlider.value -= 0.2f;
            FlashingObject myScript = GetComponent<FlashingObject>();
            mySpriteRenderer.enabled = true;
            timer = rate;
            startBlinking = true;
        }
    }

    private void BlinkingObject()
    {
        timer += Time.deltaTime;
        if (timer >= rate)
        {
            mySpriteRenderer.enabled = !mySpriteRenderer.enabled;
            timer = 0;
        }
    }
}
