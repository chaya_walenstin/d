﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DizzyJump : MonoBehaviour {

    private Transform myTransform;
    private Rigidbody2D myRigidbody;
    private Animator myAnimator;
    private float rate = 0.5f;
    private float timer;

    // Use this for initialization
    void Start () {
        myTransform = GetComponent<Transform>();
        myRigidbody = GetComponent<Rigidbody2D>();
        myAnimator = GetComponent<Animator>();
        timer = rate;
	}
	
	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;
        if (Input.GetKeyDown(KeyCode.Space) && timer>= rate)
        {
            //myTransform.position = new Vector2(myTransform.position.x, myTransform.position.y +2f);
            myAnimator.SetTrigger("jump");
            myTransform.Translate(Vector3.up * Time.deltaTime * 200);
            timer = 0;
        }
	}
}
