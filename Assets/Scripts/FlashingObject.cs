﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashingObject : MonoBehaviour {

    bool flickerOn = true;
    float rate = 3f;
    float timer = 0;

    private void Start()
    {
        timer = rate;
    }

    void Update()
    {
        timer += Time.deltaTime;
        if (timer >= rate)
        {
            flickerOn = !flickerOn;
            gameObject.SetActive(flickerOn);
            timer = 0;
        }
    }
}
