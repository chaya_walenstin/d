﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OnWin : MonoBehaviour {

    public Rigidbody2D myRigidBody;
    public GameObject winMessage;
    public GameObject winImage;
    public GameObject conffety;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Time.timeScale = 0;
        //winMessage.SetActive(true);
        //winImage.SetActive(true);
        //conffety.SetActive(true);
        Invoke("loadWinScene", 2);
    }

    void loadWinScene()
    {
        SceneManager.LoadScene(3);
    }

}
