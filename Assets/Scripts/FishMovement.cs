﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FishMovement : MonoBehaviour {

    private Rigidbody2D myRightBody2D;
    public Transform borderLeft;
    public Transform borderRight;
    private Transform myTransform;
    private SpriteRenderer mySpriteRenderer;
    float rate = 0.06f;
    float timer;
    public bool startBlinking = false;
    public int speed = 5;
    float stopTimer = 0;


    void Start()
    {
        myRightBody2D = GetComponent<Rigidbody2D>();
        myTransform = GetComponent<Transform>();
        mySpriteRenderer = GetComponent<SpriteRenderer>();
        if (gameObject.transform.position.x <= borderLeft.position.x)
        {
            myRightBody2D.velocity = Vector2.right * speed;
            mySpriteRenderer.flipX = !mySpriteRenderer.flipX;
        }
        if (gameObject.transform.position.x >= borderRight.position.x)
        {
            myRightBody2D.velocity = Vector2.left * speed;
            mySpriteRenderer.flipX = !mySpriteRenderer.flipX;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject.transform.position.x <= borderLeft.position.x)
        {
            myRightBody2D.velocity = Vector2.right * speed;
            mySpriteRenderer.flipX = !mySpriteRenderer.flipX;
        }
        if (gameObject.transform.position.x >= borderRight.position.x)
        {
            myRightBody2D.velocity = Vector2.left * speed;
            mySpriteRenderer.flipX = !mySpriteRenderer.flipX;
        }
    }
}
