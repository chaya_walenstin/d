﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDestroy : MonoBehaviour {

    public GameObject bubble;

    public float destroyTime = 12.0f;
    // Use this for initialization
    void Start()
    {
        Destroy(bubble, destroyTime);
    }

    // Update is called once per frame
    void Update () {
		
	}
}
