﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class MoveScenes : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void GameBeginning()
    {
        print(" SceneManager.LoadScene(0)");
        SceneManager.LoadScene(0);
    }

    public void GameSettings()
    {
        print(" SceneManager.LoadScene(1)");
        SceneManager.LoadScene(1);
    }

    public void StartGame()
    {
        print(" SceneManager.LoadScene(2)");
        SceneManager.LoadScene(2);
    }

    public void StartGameAgain()
    {
        SceneManager.LoadScene(2);
        LifeManager.lifeCounter = 0;
        Time.timeScale = 1;
    }
}
