﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class OxygenManager : MonoBehaviour {

    private Slider mySlider;
    public ShowOxygenBottles showBottles;
    private float timer;
    private float rate = 2f;
    public GameObject oxygenBottle;
    public GameObject oxygenBottle1;
    public GameObject oxygenBottle2;
    public GameObject oxygenBottle3;

    public GameObject gameOver;

    public Text TextMessage;

    public GameObject life1;
    public GameObject life2;
    public GameObject life3;

    public GameObject dizzy;
    // Use this for initialization
    void Start () {
        timer = rate;   
        mySlider = GetComponent<Slider>();
        if (LifeManager.singelton != null)
        {
            switch (LifeManager.lifeCounter)
            {
                case 1:
                    life3.SetActive(false);
                    life2.SetActive(true);
                    life1.SetActive(true);
                    print("hide life 3");
                    break;
                case 2:
                    life3.SetActive(false);
                    life2.SetActive(false);
                    life1.SetActive(true);
                    print("hide life 2");
                    break;
                case 3:
                    life3.SetActive(false);
                    life2.SetActive(false);
                    life1.SetActive(false);
                    print("hide life 1");
                    break;
                default:
                    break;
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;

        if (timer >= rate && mySlider.value > 0)
        {
            mySlider.value -= 0.01f;

            if (mySlider.value <= 0.4)
            {
                oxygenBottle.SetActive(true);
                oxygenBottle1.SetActive(true);
                oxygenBottle2.SetActive(true);
                oxygenBottle3.SetActive(true);
                TextMessage.enabled = true;
            }
            else if (mySlider.value >= 0.9)
            {
                oxygenBottle.SetActive(false);
                oxygenBottle1.SetActive(false);
                oxygenBottle2.SetActive(false);
                oxygenBottle3.SetActive(false);
            }
            timer = 0;

            showBottles.ActiveBottle();
        }
        if (mySlider.value <= 0)
        {
            LifeManager.singelton.AddLife();
            if (LifeManager.lifeCounter >= 3)
            {
                life3.SetActive(false);
                life2.SetActive(false);
                life1.SetActive(false);
                print("hide life 1");
                Time.timeScale = 0;
                gameOver.SetActive(true);
                dizzy.SetActive(false);
            }
            else
            {
                SceneManager.LoadScene(2);
            }
        }
    }
}
