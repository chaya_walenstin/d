﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class dizzyStartWalk : MonoBehaviour {

    private Rigidbody2D dizzyRigitBody;
    public float speed;
    private Transform myTransform;
    // Use this for initialization
    void Start () {
        dizzyRigitBody = GetComponent<Rigidbody2D>();
        myTransform = GetComponent<Transform>();
    }
	
	// Update is called once per frame
	void Update () {
        speed = 3 * (3 - myTransform.localScale.x);
        dizzyRigitBody.velocity = Vector2.left * speed;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        SceneManager.LoadScene(2);
    }
}
