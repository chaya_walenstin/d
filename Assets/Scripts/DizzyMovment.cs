﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DizzyMovment : MonoBehaviour
{

    public Camera mainCamera;

    public Transform mainCameraTransform;
    private Transform myTransform;
    public Transform borderLeft;
    public Transform borderRight;
    private Animator myAnimator;

    private float speed = 10;
    private float velocitySpeed = 5f;
    private float halfSpaceShipSizeX = 0f;
    private float halfSpaceShipSizeY = 0f;
    private float cameraHigh;
    private float cameraWidth;

    private SpriteRenderer mySpriteRendere;
    private Rigidbody2D myRightBody;
    private AudioSource jumpSound;

    private float timer;
    private float rate = 0.5f;
    private float jumpForce = 10f;
    private float horizontalForce = 10f;
    private float jumpDelay = 0.3f;
    private bool isJumping;

    [SerializeField] private LayerMask bubbleLayer;

    // Use this for initialization
    void Start()
    {
        myTransform = GetComponent<Transform>();
        mySpriteRendere = GetComponent<SpriteRenderer>();
        myRightBody = GetComponent<Rigidbody2D>();
        myAnimator = GetComponent<Animator>();
        jumpSound = GetComponent<AudioSource>();

        halfSpaceShipSizeX = mySpriteRendere.bounds.size.x / 2f;
        halfSpaceShipSizeY = mySpriteRendere.bounds.size.y / 2f;

        cameraHigh = mainCamera.orthographicSize * 2f;
        cameraWidth = cameraHigh * ((float)Screen.width / (float)Screen.height);

        borderLeft.position = mainCameraTransform.position - new Vector3(cameraWidth / 2, cameraHigh / 2, 0);
        borderRight.position = mainCameraTransform.position + new Vector3(cameraWidth / 2, cameraHigh / 2, 0);

        borderLeft.Translate(halfSpaceShipSizeX, halfSpaceShipSizeY, 0);
        borderRight.Translate(-halfSpaceShipSizeX, -halfSpaceShipSizeY, 0);

        timer = rate;
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit2D hitInfo = Physics2D.Raycast(myTransform.position, Vector2.down, 2,bubbleLayer);
        if(hitInfo.collider == null)
        {
            if(!isJumping)
            {
                applyGravity();
            }
        }

        timer += Time.deltaTime;
        if (Input.GetKey(KeyCode.RightArrow))
        {
            myAnimator.SetBool("isWalk", true);
            myRightBody.velocity = new Vector2(horizontalForce, myRightBody.velocity.y);
            mySpriteRendere.flipX = true;
        }

        else if (Input.GetKey(KeyCode.LeftArrow))
        {
            myAnimator.SetBool("isWalk", true);
            myRightBody.velocity = new Vector2(-horizontalForce, myRightBody.velocity.y);
            mySpriteRendere.flipX = false;
        }
        else
        {
            myAnimator.SetBool("isWalk", false);
        }

        if (Input.GetKeyDown(KeyCode.Space) && timer >= rate)
        {
            myAnimator.SetTrigger("jump");
            jumpSound.Play();
            myRightBody.velocity = new Vector2(myRightBody.velocity.x, jumpForce);
            timer = 0;
            Invoke("applyGravity", jumpDelay);
            Invoke("stopJumping", jumpDelay);
            isJumping = true;
        }
    }

    void applyGravity()
    {
        myRightBody.velocity = Vector2.down * velocitySpeed;
    }

    void stopJumping()
    {
        isJumping = false;
    }


}
