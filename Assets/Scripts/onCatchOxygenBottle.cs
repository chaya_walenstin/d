﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class onCatchOxygenBottle : MonoBehaviour {

    //public Transform oxygenValue;
    public Slider oxygenValue;
    private AudioSource fillSound;

    // Use this for initialization
    void Start () {
        fillSound = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Bottle"))
        {
            oxygenValue.value = 1f;
            collision.gameObject.SetActive(false);
        }
    }
}
