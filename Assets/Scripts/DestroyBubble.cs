﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyBubble : MonoBehaviour {
    
    public float timer;
    private SpriteRenderer mySpriteRenderer;
    public Sprite burstSprite;
    private CircleCollider2D circleCollider;

    // Use this for initialization
    void Start () {
        mySpriteRenderer = GetComponent<SpriteRenderer>();
        circleCollider = GetComponent<CircleCollider2D>();
        timer = 0;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    

    private void OnCollisionStay2D(Collision2D collision)
    {
        timer += Time.deltaTime;
        if (timer >= 1.0f)
        {
            mySpriteRenderer.sprite = burstSprite;
            circleCollider.isTrigger = true;
            Destroy(gameObject, 0.3f);
        }
    }
}
