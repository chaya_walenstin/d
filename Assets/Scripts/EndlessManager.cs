﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndlessManager : MonoBehaviour {

    public Transform otherTransform;
    public SpriteRenderer bgSpriterenderer;
    private float blockHight;
    public EndlessManager otherBlockManager;
    //public Transform dizyTransform;
    //private float lastY;
    // Use this for initialization

    public bool isReady = true;
	void Start () {
        blockHight = bgSpriterenderer.bounds.size.y;
        //lastY = dizyTransform.position.y;
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        if(isReady)
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                //if (lastY < dizyTransform.position.y)
                //{
                    otherTransform.Translate(0, blockHight * 2, 0);
                //}
                //else
                //{
                //    otherTransform.Translate(0, -blockHight * 2, 0);
                //}
               
                isReady = false;
                otherBlockManager.isReady = true;
            }

           
        }


    }
}
