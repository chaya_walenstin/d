﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinFlash : MonoBehaviour {
    float rate = 0.06f;
    float timer;
    public bool startBlinking = false;
    float stopTimer = 0;
    

	
	// Update is called once per frame
	void Update () {
        if (startBlinking)
        {
            stopTimer += Time.deltaTime;
            BlinkingObject();
        }
        if (stopTimer >= 2f)
        {
            stopTimer = 0;
            gameObject.SetActive(true);
            startBlinking = false;
        }
    }


    private void BlinkingObject()
    {
        timer += Time.deltaTime;
        if (timer >= rate)
        {
            gameObject.SetActive(!gameObject.active);
            timer = 0;
        }
    }

}
