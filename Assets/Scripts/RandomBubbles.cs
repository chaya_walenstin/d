﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomBubbles : MonoBehaviour {

    public GameObject prefab;
    private Transform startPosition;
    public Transform bubbleTransform;
    private float size;
    public float rateBubbleCreation = 0.3f;
    public float timer = 0;
    public Transform rightTransform;
    public Transform leftTransform;

    

	// Use this for initialization
	void Start () {
		startPosition = GetComponent<Transform>();
        timer = rateBubbleCreation;
	}
	
	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;
      
        if(timer >= rateBubbleCreation)
        {
            size = Random.Range(0.4f, 1.2f);
            bubbleTransform.localScale = new Vector3(size, size, 0);
            Vector3 newPosition = new Vector3(Random.Range(leftTransform.position.x, rightTransform.position.x), startPosition.position.y, 0);
            Instantiate(prefab, newPosition, Quaternion.identity);
            timer = 0;
        }
	}
}
