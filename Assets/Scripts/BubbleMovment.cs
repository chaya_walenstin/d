﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BubbleMovment : MonoBehaviour {

    private Rigidbody2D bubbleRightBody2D;
    public float speed;
    private Transform myTransform;
    // Use this for initialization
    void Start () {
        bubbleRightBody2D = GetComponent<Rigidbody2D>();
        myTransform = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
        speed = 3 * (3 - myTransform.localScale.x);
        bubbleRightBody2D.velocity = Vector2.up * speed;
	}
}
