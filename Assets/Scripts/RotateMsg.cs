﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateMsg : MonoBehaviour {

    public bool startBlinking;
    private float stopTimer = 0f;
    private float timer = 0f;
    private float rate = 0.06f;
    public GameObject gameOverObj;

    // Use this for initialization
    void Start ()
    {
        timer = rate;
        if (startBlinking)
        {
            stopTimer += Time.deltaTime;
            BlinkingObject();
        }
        if (stopTimer >= 2f)
        {
            stopTimer = 0;
            gameOverObj.SetActive(true);
            startBlinking = false;
        }

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void BlinkingObject()
    {
        timer += Time.deltaTime;
        if (timer >= rate)
        {
            gameOverObj.SetActive(!gameOverObj.activeSelf);
            timer = 0;
        }
    }
}
